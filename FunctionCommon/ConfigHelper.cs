﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionCommon
{
    public class ConfigHelper
    {
        public static string GetConfiginfo(string key) 
        {
            try
            {
                return ConfigurationManager.AppSettings[key].ToString().Trim();
            }
            catch (Exception)
            {

                return "";
            }
          
        }
    }
}
