﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer.xk
{
    /// <summary>
    /// 厨师接口
    /// </summary>
  public  interface ICooker
    {
        void Cook();
    }
}
