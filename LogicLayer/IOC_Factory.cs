﻿using FunctionCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer
{
    public   class IOC_Factory
    {
        /// <summary>
        /// 得到厨师
        /// </summary>
        /// <typeparam name="T">你想实现的接口</typeparam>
        /// <param name="key">指定要什么菜</param>
        /// <returns>关于这道菜对应的厨师接口</returns>
        public static T GetCooker<T>(string key) 
        {
           
           string classname = ConfigHelper.GetConfiginfo(key);
           string assemblyname = classname.Substring(0,classname.LastIndexOf('.'));
           try
           {
               return (T)Assembly.Load(assemblyname).CreateInstance(classname);
           }
           catch (Exception)
           {
               return default(T);
           }
          
        }

    }
}
