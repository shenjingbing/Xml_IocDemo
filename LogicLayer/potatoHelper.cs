﻿using LogicLayer.xk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer.rabbit
{
    /// <summary>
    /// 土豆助手类
    /// </summary>
    public class potatoHelper:ICooker
    {

        public void Cook()
        {
            Console.WriteLine("您要的土豆已经做好!");
        }
    }
}
