﻿using LogicLayer.xk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicLayer.rabbit
{
    /// <summary>
    /// 西红柿助手类
    /// </summary>
    class tomatoHelper:ICooker
    {

        public void Cook()
        {
            Console.WriteLine("您要的西红柿已经做好啦！");
        }
    }
}
