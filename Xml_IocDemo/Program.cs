﻿using LogicLayer;
using LogicLayer.rabbit;
using LogicLayer.xk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xml_IocDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //面向类编程，类与类之间耦合
            potatoHelper patatocooker = new potatoHelper();
            patatocooker.Cook();
            Console.WriteLine("=========================");
            

            //面向接口编程，类只与接口打交道
            ICooker Cooker = IOC_Factory.GetCooker<ICooker>("土豆");
            Cooker.Cook();
            Console.Read();
        }
    }
}
